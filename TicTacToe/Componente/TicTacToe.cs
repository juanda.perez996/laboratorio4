﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Componente
{
    public partial class TicTacToe : UserControl
    {
        bool jugador;
        int contador;
        public TicTacToe( )
        {
            InitializeComponent();
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            
        }

        private void turno() 
        {
            if (jugador)
            {
                jugador = false;
            }
            else 
            {
                jugador = true;
            }
        }

        private void conteo() 
        {
            contador++;
        }

        private int ganador() 
        {
            if (contador > 4)
            {
                if
                    ((button1.Text.Equals("X") && button2.Text.Equals("X") && button3.Text.Equals("X"))
                    || (button4.Text.Equals("X") && button5.Text.Equals("X") && button6.Text.Equals("X"))
                    || (button7.Text.Equals("X") && button8.Text.Equals("X") && button9.Text.Equals("X")))
                {
                    return 1;
                }
                else if
                  ((button1.Text.Equals("O") && button2.Text.Equals("O") && button3.Text.Equals("O"))
                  || (button4.Text.Equals("O") && button5.Text.Equals("O") && button6.Text.Equals("O"))
                  || (button7.Text.Equals("O") && button8.Text.Equals("O") && button9.Text.Equals("O")))
                {
                    return 1;
                }
                else if
                  ((button1.Text.Equals("X") && button4.Text.Equals("X") && button7.Text.Equals("X"))
                  || (button2.Text.Equals("X") && button5.Text.Equals("X") && button8.Text.Equals("X"))
                  || (button3.Text.Equals("X") && button6.Text.Equals("X") && button9.Text.Equals("X")))
                {
                    return 1;
                }
                else if
                   ((button1.Text.Equals("O") && button4.Text.Equals("O") && button7.Text.Equals("O"))
                   || (button2.Text.Equals("O") && button5.Text.Equals("O") && button8.Text.Equals("O"))
                   || (button3.Text.Equals("O") && button6.Text.Equals("O") && button9.Text.Equals("O")))
                {
                    return 1;
                }
                else if
                   ((button1.Text.Equals("O") && button5.Text.Equals("O") && button9.Text.Equals("O"))
                  || (button3.Text.Equals("O") && button5.Text.Equals("O") && button7.Text.Equals("O")))
                {
                    return 1;
                }
                else if
                  ((button1.Text.Equals("X") && button5.Text.Equals("X") && button9.Text.Equals("X"))
                 || (button3.Text.Equals("X") && button5.Text.Equals("X") && button7.Text.Equals("X")))
                {
                    return 1;
                }
                else if (contador > 8) 
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button1.Text = "X";
                turno();
            }
            else 
            {
                button1.Text = "O";
                turno();
            }
            button1.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2) 
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button2.Text = "X";
                turno();
            }
            else
            {
                button2.Text = "O";
                turno();
            }
            button2.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button3.Text = "X";
                turno();
            }
            else
            {
                button3.Text = "O";
                turno();
            }
            button3.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button4.Text = "X";
                turno();
            }
            else
            {
                button4.Text = "O";
                turno();
            }
            button4.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button5.Text = "X";
                turno();
            }
            else
            {
                button5.Text = "O";
                turno();
            }
            button5.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button6.Text = "X";
                turno();
            }
            else
            {
                button6.Text = "O";
                turno();
            }
            button6.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button7.Text = "X";
                turno();
            }
            else
            {
                button7.Text = "O";
                turno();
            }
            button7.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button8.Text = "X";
                turno();
            }
            else
            {
                button8.Text = "O";
                turno();
            }
            button8.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            conteo();
            if (jugador)
            {
                button9.Text = "X";
                turno();
            }
            else
            {
                button9.Text = "O";
                turno();
            }
            button9.Enabled = false;
            if (ganador() == 1)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS GANADOR";
            }
            else if (ganador() == 2)
            {
                this.CausesValidation = false;
                label1.Text = "TENEMOS EMPATE";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (this.CausesValidation == false)
            {
                label1.Text = "";
                button1.Text = "";
                button2.Text = "";
                button3.Text = "";
                button4.Text = "";
                button5.Text = "";
                button6.Text = "";
                button7.Text = "";
                button8.Text = "";
                button9.Text = "";
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = true;
                button7.Enabled = true;
                button8.Enabled = true;
                button9.Enabled = true;
                contador = 0;
            }
        }
    }
}
