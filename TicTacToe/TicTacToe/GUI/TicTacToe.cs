﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToe.GUI;

namespace TicTacToe
{
    public partial class TicTacToe : Form
    {
        LinkedList<Usuario> usuarios = new LinkedList<Usuario>();
        bool ganador = true;
        public TicTacToe()
        {
            InitializeComponent();
            
        }

        private void TicTacToe_Load(object sender, EventArgs e)
        {
            ticTacToe1.Enabled = false;
        }

        private void initUsers() 
        {
            addUsers au = new addUsers(usuarios);
            au.Show();
        }

        private void ticTacToe1_Click(object sender, EventArgs e)
        {
            
            
        }

        private void ticTacToe1_Load(object sender, EventArgs e)
        {
            if (ticTacToe1.CausesValidation = false)
            {
                if (ganador = true)
                {
                    lblGanador.Text = lblj1.Text;
                    ganador = false;
                }
                else
                {
                    lblGanador.Text = lblj2.Text;
                    ganador = false;
                }
            }

            if (ganador)
            {
                ganador = false;
            }
            else
            {
                ganador = true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            initUsers();
            ticTacToe1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (usuarios.First == null)
            {
                MessageBox.Show("Debe ingresar los usuarios para jugar");
            }
            else 
            {
                lblj1.Text = usuarios.First.Value.nombre + " - " + usuarios.First.Value.nombreUsuario;
                lblj2.Text = usuarios.Last.Value.nombre + " - " + usuarios.Last.Value.nombreUsuario;
            }
        }

        private void ticTacToe1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }
    }
}
