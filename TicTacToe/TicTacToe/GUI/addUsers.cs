﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe.GUI
{
    public partial class addUsers : Form
    {
        LinkedList<Usuario> usuarios = new LinkedList<Usuario>();

        public addUsers(LinkedList<Usuario> usuarios)
        {
            InitializeComponent();
            this.usuarios = usuarios;
        }

        private void addUsers_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Usuario j1 = new Usuario(int.Parse(txtJ1Cedula.Text),txtJ1Nombre.Text,txtJ1Usuario.Text);
            Usuario j2 = new Usuario(int.Parse(txtJ2Cedula.Text), txtJ2Nombre.Text, txtJ2Usuario.Text);
            usuarios.AddLast(j1);
            usuarios.AddLast(j2);
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
