﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Persona
    {
        public Persona()
        {
        }

        public Persona(int cedula, string nombre)
        {
            this.cedula = cedula;
            this.nombre = nombre;
        }

        public int cedula { get; private set; }
        public string nombre { get; private set; }
    }
}
