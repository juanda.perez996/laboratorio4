﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Usuario : Persona
    {
        public Usuario()
        {
        }

        public Usuario(int cedula, string nombre, string nombreUsuario)
            : base(cedula, nombre)
        {
            
            this.nombreUsuario = nombreUsuario;
        }

        public string nombreUsuario { get; private set; }
    }
}
